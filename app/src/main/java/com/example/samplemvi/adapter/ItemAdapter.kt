package com.example.samplemvi.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.bumptech.glide.Glide
import com.example.samplemvi.utils.Constants
import com.example.samplemvi.data.model.ItemFilm
import com.example.samplemvi.databinding.ItemListBinding

class ItemAdapter(private val context: Context, private val movieClickListener: MovieClickListener) : ListAdapter<ItemFilm, ItemViewHolder>(ItemAdapter) {
    companion object : DiffUtil.ItemCallback<ItemFilm>() {
        override fun areItemsTheSame(oldItem: ItemFilm, newItem: ItemFilm): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: ItemFilm, newItem: ItemFilm): Boolean {
            return oldItem == newItem
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListBinding.inflate(inflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val binding = holder.binding as ItemListBinding
        val currentFilm = getItem(position)
        val url = Constants.DEFAULT_IMAGE_URL + currentFilm.poster_path
        binding.apply {
            title.text = currentFilm.title
            Glide.with(context)
                .load(url)
                .centerCrop()
                .into(imageView)
        }
        binding.itemView.setOnClickListener {
            movieClickListener.onMovieClickListener(currentFilm.id)
        }
    }
}

class ItemViewHolder(val binding: ViewBinding) : RecyclerView.ViewHolder(binding.root)

interface MovieClickListener {
    fun onMovieClickListener(id: Int)
}
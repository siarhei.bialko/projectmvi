package com.example.samplemvi.ui

import android.os.Bundle
import com.example.samplemvi.base.UiEffect
import com.example.samplemvi.base.UiEvent
import com.example.samplemvi.base.UiState
import com.example.samplemvi.data.model.ItemFilm
import com.example.samplemvi.data.model.DetailFilm

class MainContract {

    sealed class Event : UiEvent {
        object OnLoadMoviesClicked : Event()
        object OnNavigationWithoutParamsClicked : Event()
        data class OnMovieClicked(val id: Int) : Event()
        data class OnBtnRetryClicked(val id: Int) : Event()
    }

    data class State(
        val isLoading: Boolean = false,
        val movies : List<ItemFilm> = emptyList(),
        val error: String? = null,
        val movieDetail: DetailFilm? = null,
        val errorDetail: String? = null,
        val errorDetailRes: Int? = null,
    ) : UiState

    sealed class Effect : UiEffect {
        data class ShowToast(val message : String) : Effect()
        data class NavigateTo(val action: Int, val bundle: Bundle? = null) : Effect()
    }

}
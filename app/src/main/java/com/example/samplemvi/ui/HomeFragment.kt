package com.example.samplemvi.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.samplemvi.R
import com.example.samplemvi.adapter.ItemAdapter
import com.example.samplemvi.adapter.MovieClickListener
import com.example.samplemvi.base.IView
import com.example.samplemvi.databinding.HomeFragmentBinding
import kotlinx.coroutines.flow.collect

class HomeFragment : Fragment(R.layout.home_fragment), MovieClickListener, IView<MainContract.State> {

    private var _binding: HomeFragmentBinding? = null
    private val binding get() = _binding!!

    private val viewModel : MainViewModel by activityViewModels()

    private lateinit var mAdapter : ItemAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = HomeFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecyclerView()
        initObservers()
        initListeners()
    }

    private fun setupRecyclerView() {
        mAdapter = ItemAdapter(requireContext(), this)
        binding.recyclerView.adapter = mAdapter
    }

    override fun onMovieClickListener(id: Int) {
        viewModel.setEvent(MainContract.Event.OnMovieClicked(id))
    }

    private fun initListeners() {
        binding.btnLoadMovies.setOnClickListener {
            viewModel.setEvent(MainContract.Event.OnLoadMoviesClicked)
        }
        binding.btnRefreshMovies.setOnClickListener {
            viewModel.setEvent(MainContract.Event.OnLoadMoviesClicked)
        }
        binding.btnNavigate.setOnClickListener {
            viewModel.setEvent(MainContract.Event.OnNavigationWithoutParamsClicked)
        }
    }

    private fun initObservers() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                render(it)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.effect.collect {
                when (it) {
                    is MainContract.Effect.ShowToast -> {
                        showToast(it.message)
                    }
                    is MainContract.Effect.NavigateTo -> {
                        findNavController().navigate(it.action, it.bundle)
                    }
                }
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText( requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun render(state: MainContract.State) {
        mAdapter.submitList(state.movies)
        binding.apply {
            progressBar.isVisible = state.isLoading
            btnRefreshMovies.isVisible = state.movies.isNotEmpty()
            btnNavigate.isVisible = state.movies.isNotEmpty()
            btnLoadMovies.isVisible = state.movies.isEmpty()
            txtWelcome.isVisible = state.movies.isEmpty()
        }
    }
}
package com.example.samplemvi.ui

import androidx.core.os.bundleOf
import androidx.lifecycle.viewModelScope
import com.example.samplemvi.R
import com.example.samplemvi.base.BaseViewModel
import com.example.samplemvi.repository.MainRepository
import com.example.samplemvi.utils.ResultWrapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository
) : BaseViewModel<MainContract.Event, MainContract.State, MainContract.Effect>() {

    override fun createInitialState(): MainContract.State = MainContract.State()

    override fun handleEvent(event: MainContract.Event) {
        when (event) {
            is MainContract.Event.OnLoadMoviesClicked -> getPopularMovies()
            is MainContract.Event.OnMovieClicked -> {
                viewModelScope.launch(Dispatchers.Main) {
                    delay(100)
                    val bundle = bundleOf("id" to event.id)
                    setEffect ( MainContract.Effect.NavigateTo(R.id.action_homeFragment_to_detailFragment, bundle) )
                    getDetailMovie(event.id)
                }
            }
            is MainContract.Event.OnNavigationWithoutParamsClicked -> {
                setEffect ( MainContract.Effect.NavigateTo(R.id.action_homeFragment_to_emptyFragment) )
            }
            is MainContract.Event.OnBtnRetryClicked -> getDetailMovie(event.id)
        }
    }

    private fun getDetailMovie(id: Int) {
        viewModelScope.launch {
            when(val result = mainRepository.getDetailFilm(id, Dispatchers.IO) ) {
                is ResultWrapper.Success -> {
                    setState { copy(movieDetail = result.value, errorDetail = null, errorDetailRes = null) }
                }
                is ResultWrapper.GenericError -> {
                    setState { copy(errorDetail = result.code.toString(), errorDetailRes = null) }
                }
                is ResultWrapper.NetworkError -> {
                    setState { copy(errorDetailRes = R.string.default_error, errorDetail = null ) }
                }
            }
        }
    }

    private fun getPopularMovies() {
        viewModelScope.launch {
            setState { copy(isLoading = true) }
            when(val result = mainRepository.getListPopular(dispatcher = Dispatchers.IO) ) {
                is ResultWrapper.Success -> {
                    setState { copy(isLoading = false, movies = result.value.results) }
                }
                is ResultWrapper.GenericError -> {
                    setState { copy(isLoading = false) }
                    setEffect ( MainContract.Effect.ShowToast(result.code.toString()) )
                }
                is ResultWrapper.NetworkError -> {
                    setState { copy(isLoading = false) }
                    setEffect ( MainContract.Effect.ShowToast(result.toString()) )
                }
            }
        }
    }
}
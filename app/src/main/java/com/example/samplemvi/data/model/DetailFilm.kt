package com.example.samplemvi.data.model

data class DetailFilm(
    val backdrop_path: String,
    val overview: String,
    val poster_path: Any?,
    val release_date: String,
    val title: String,
    val vote_average: Double,
    val vote_count: Int
)
package com.example.samplemvi.data

import com.example.samplemvi.data.model.ListFilm
import com.example.samplemvi.data.model.DetailFilm
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiTMDB {
    @GET("movie/popular")
    suspend fun getListPopular(@Query("page") page: Int?): ListFilm

    @GET("movie/{movie_id}")
    suspend fun getDetailFilm(@Path("movie_id") id: Int): DetailFilm
}
package com.example.samplemvi.base

interface IView<S: UiState> {
    fun render(state: S)
}
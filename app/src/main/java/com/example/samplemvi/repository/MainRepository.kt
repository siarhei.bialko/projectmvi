package com.example.samplemvi.repository

import com.example.samplemvi.data.ApiTMDB
import com.example.samplemvi.data.model.ListFilm
import com.example.samplemvi.data.model.DetailFilm
import com.example.samplemvi.utils.ResultWrapper
import com.example.samplemvi.utils.safeApiCall
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject

class MainRepository @Inject constructor(
    private val api: ApiTMDB
) {
    suspend fun getListPopular(page: Int = 1, dispatcher: CoroutineDispatcher): ResultWrapper<ListFilm> =
        withContext(dispatcher) { safeApiCall { api.getListPopular(page) } }

    suspend fun getDetailFilm(id:Int, dispatcher: CoroutineDispatcher): ResultWrapper<DetailFilm> =
        withContext(dispatcher) { safeApiCall{ api.getDetailFilm(id) } }

}
